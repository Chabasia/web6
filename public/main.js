window.addEventListener("DOMContentLoaded", function () {
    const OPTION_PRICE_ONE = 1000;
    const OPTION_PRICE_TWO = 3000;
    const OPTION_PRICE_THREE = 10000;
    const NOTHING_EXTRA_PRICE = 0;
    const RADIO_ONE = 0;
    const RADIO_TWO = 500;
    const CHECKBOX = 50;
    let price = OPTION_PRICE_ONE;
    let extraPrice = RADIO_ONE;
    let result;
    let calc = document.getElementById("calcin");
    let myNum = document.getElementById("my-num");
    let resultSpan = document.getElementById("result");
    let mySel = document.getElementById("my-sel");
    let radiosDiv = document.getElementById("my-radio-div");
    let radios = document.querySelectorAll("#my-radio-div input[type=radio]");
    let checkboxDiv = document.getElementById("my-checkbox-div");
    let checkbox = document.getElementById("my-checkbox");
    mySel.addEventListener("change", function (event) {
        let option = event.target;
        if (option.value === "1") {
            extraPrice = RADIO_ONE;
            price = OPTION_PRICE_ONE;
        }
        if (option.value === "2") {
            radiosDiv.classList.remove("d-none");
            checkboxDiv.classList.add("d-none");
            extraPrice = RADIO_ONE;
            price = OPTION_PRICE_TWO;
            document.getElementById("my-radio1").checked = true;
        }
        if (option.value === "3") {
            extraPrice = RADIO_ONE;
            price = OPTION_PRICE_THREE;
            checkbox.checked = false;
        }
    });
    radios.forEach(function (currentRadio) {
        currentRadio.addEventListener("change", function (event) {
            let radio = event.target;
            if (radio.value === "r1") {
                extraPrice = RADIO_ONE;
            }
            if (radio.value === "r2") {
                extraPrice = RADIO_TWO;
            }
        });
    });
    checkbox.addEventListener("change", function () {
        if (checkbox.checked) {
            extraPrice = CHECKBOX;
        } else {
            extraPrice = NOTHING_EXTRA_PRICE;
        }
    });
    calc.addEventListener("change", function () {
        if (myNum.value < 1) {
            myNum.value = 1;
        }
        result = (myNum.value * price) + extraPrice * myNum.value ;
        resultSpan.innerHTML = result;
    });
});
